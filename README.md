# SSI Beacon
SSI beacon provides a simulation of an alternative model for discovering and sharing genomic data. This model relies on blockchain-based self-sovereign identity (SSI) for authenticating both researchers and datasets. In addition, it demostrate how privacy-preserving analysis can be performed over a DID-authenticated channel. 


Details of the workflow can be found in the paper: 
Blockchain-based SSI and Homomorphic Encryption for Secure Sharing and Processing of Genomic Data. 