# Listener functions

from termcolor import colored
import json

# Receive connection messages
def _connections_handler(payload):
    state = payload['state']
    connection_id = payload["connection_id"]
    their_role = payload["their_role"]
    routing_state = payload["routing_state"]
    
    print("----------------------------------------------------------")
    print("Connection Webhook Event Received")
    print("Connection ID : ", connection_id)
    print("State : ", state)
    print("Routing State : ", routing_state)
    print("Their Role : ", their_role)
    print("----------------------------------------------------------")

    if state == "invitation":
        # Your business logic
        print("invitation")
    elif state == "request":
        # Your business logic
        print("request")

    elif state == "response":
        # Your business logic
        print("response")
    elif state == "active":
        # Your business logic
        print(colored("Connection ID: {0} is now active.".format(connection_id), "green", attrs=["bold"]))

# handle pk messages
def _pk_messages_handler(payload):
    connection_id = payload["connection_id"]
    content = payload["content"]
    try:
        content_json = json.loads(content)
        if content_json['type'] == 'public_key':
            print("Public Key Message Handler", payload, connection_id)
            pk_file = "publickey.txt"
            pk = content_json["data"]
            try:
                f = open(pk_file, "w+")
                json.dump(pk, f)
                f.close()
                print("public key saved to file: publickey.txt")
            except Exception as e:
                print("Error writing file", e)
                return
    except Exception as e:
        print("message not handeled by pk message handler")
        return

def _phe_messages_handler(payload):
    connection_id = payload["connection_id"]
    content = payload["content"]
    try:
        content_json = json.loads(content)
        if content_json["type"] == "phe":
            print("PHE encrypted data Message Handler", payload, connection_id)
            phe_file = "./enc_datasets/phe_" + connection_id + ".txt"
            pk = content_json["data"]
            try:
                f = open(phe_file, "a")
                json.dump(pk, f)
                f.close()
            except Exception as e:
                print("Error writing file", e)
                return
    except Exception as e:
        print("message not handeled by phe message handler")
        return


def _issuer_handler(payload):
    connection_id = payload['connection_id']
    exchange_id = payload['credential_exchange_id']
    state = payload['state']
    role = payload['role']
    print("\n---------------------------------------------------\n")
    print("Handle Issue Credential Webhook")
    print(f"Connection ID : {connection_id}")
    print(f"Credential exchange ID : {exchange_id}")
    print("Agent Protocol Role : ", role)
    print("Protocol State : ", state )
    print("\n---------------------------------------------------\n")


    if state == "offer_sent":
        proposal = payload["credential_proposal_dict"]
        attributes = proposal['credential_proposal']['attributes']

        print(f"Offering credential with attributes  : {attributes}")
        ## YOUR LOGIC HERE
    elif state == "request_received":
        print("Request for credential received")
        ## YOUR LOGIC HERE
    elif state == "credential_sent":
        print("Credential Sent")
        ## YOUR LOGIC HERE
        
def _proof_handler(payload):
    role = payload["role"]
    connection_id = payload["connection_id"]
    pres_ex_id = payload["presentation_exchange_id"]
    state = payload["state"]
    print("\n---------------------------------------------------------------------\n")
    print("Handle present-proof")
    print("Connection ID : ", connection_id)
    print("Presentation Exchange ID : ", pres_ex_id)
    print("Protocol State : ", state)
    print("Agent Role : ", role)
    print("Initiator : ", payload["initiator"])
    print("\n---------------------------------------------------------------------\n")


    if state == "request_sent":
        print("Presentation Request\n")
        print(payload["presentation_request"])
        print("\nThe presentation request is encoded in base64 and packaged into a DIDComm Message\n")
        print(payload["presentation_request_dict"])
        print("\nNote the type defines the protocol present-proof and the message request-presentation\n")
        print(payload["presentation_request_dict"]["@type"])
    elif state == "presentation_received":
        print("Presentation Received")
        print("We will not go into detail on this payload as it is comparable to the presentation_sent we looked at in the earlier cell.")
        print("This is the full payload\n")
        print(payload)
    else:
        print("Paload \n")
        print(payload)