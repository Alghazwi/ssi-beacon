# Helper functions

import numpy as np
from phe import paillier
import json
import re
import sys
import time
import pickle

def encode_encrypt():
    
    time_start = time.time()

    # get the public key from the file pk_file
    pk_file = "publickey.txt"
    file = open(pk_file)
    line = file.read().replace("\n", " ")
    file.close()

    received_dict = json.loads(line)
    pk = received_dict['public_key']
    public_key = paillier.PaillierPublicKey(n=int(pk['n']))

    # names of the input and output files
    data_file = dataset_file
    enc_data_file = f"./enc_datasets/phe_org1_{n}samples.txt"
    rows = 1

    # read the dataset in dataset_file with n number of elements as defined earlier 
    data = utils.get_data(data_file,num_elem=n)

    # map the two dataset entries to integers
    map_c, ref, alt = utils.map_data(data,num_elem=n,row_num=1)

    # save the ref and alt
    file = open('ref_alt.txt', 'wb')
    pickle.dump([ref,alt], file)
    file.close()

    # timing
    time_end = time.time()
    print('Execution time encoding dataset 1 = {} s'.format(time_end-time_start))

    time_start = time.time()

    # encrypt the dataset for the number of rows indicated 
    he_data = utils.he_data_row(map_c, public_key)
    he_data = np.asarray(he_data)
    he_data_flat = he_data.ravel()

    enc_with_one_pub_key = {}
    enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in he_data_flat]
    serialised2 = json.dumps(enc_with_one_pub_key)
    text_file = open(enc_data_file, "w")
    text_file.write(serialised2)
    text_file.close()
    print("Enctypted dataset saved it file {}\n".format(enc_data_file))

    # timing
    time_end = time.time()
    print('Execution time encrypting dataset 1 = {} s'.format(time_end-time_start))

def get_data(path,num_elem):

    """
    Function to open the .txt files of the datasets and put the data into a numpy array with 101 columns.

    Parameters:
        path (str): The path + file name of the dataset.

    Output: 
        data5 (ndarray): The dataset as a numpy array.
    """
    f = open(path, "r")
    data = f.read()
    data2 = re.split(' |\n|\n ',data)
    data3 = data2[:-1]
    data3.insert(0, 'name')
    data4=np.asarray(data3)
    data5=data4.reshape(312, num_elem+1) #31512 elements in total for both datasets
    return data5


def map_data(data,num_elem,row_num):
    """
    Function that returns the data mapped to integres (100 is the number of samples in each row - should be changed if the dataset is different)

    Parameters:
        data (ndarray): The dataset that we want to map to integers.
        num_elem (int): Number of individuals used in the dataset (so the number of samples).

    Output: 
        m_data (ndarray): The dataset with each element mapped to integers.
    """
    m_data = data[1:,1:].copy()
    row=m_data[row_num] # take the row we want
    
    if (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AG')>0) or (np.count_nonzero(row == 'AG')>0 and np.count_nonzero(row == 'GG')>0):
        ref = 0 # the corresponding ref and alt indicated with an integer
        alt = 3
        for i in range(num_elem):
            if row[i] == "AA":
                row[i]=0
            elif row[i] == 'AG':
                row[i]=1
            elif row[i] == 'GG':
                row[i]=2
    elif (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AT')>0) or (np.count_nonzero(row == 'AT')>0 and np.count_nonzero(row == 'TT')>0):
        ref = 0
        alt = 1
        for i in range(num_elem):
            if row[i] == "AA":
                row[i]=0
            elif row[i] == 'AT':
                row[i]=1
            elif row[i] == 'TT':
                row[i]=2
    elif (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AC')>0) or (np.count_nonzero(row == 'AC')>0 and np.count_nonzero(row == 'CC')>0):
        ref = 0
        alt = 2
        for i in range(num_elem):
            if row[i] == "AA":
                row[i]=0
            elif row[i] == 'AC':
                row[i]=1
            elif row[i] == 'CC':
                row[i]=2
    elif (np.count_nonzero(row == 'GG')>0 and np.count_nonzero(row == 'GT')>0) or (np.count_nonzero(row == 'GT')>0 and np.count_nonzero(row == 'TT')>0):
        ref = 3
        alt = 1
        for i in range(num_elem):
            if row[i] == "GG":
                row[i]=0
            elif row[i] == 'GT':
                row[i]=1
            elif row[i] == 'TT':
                row[i]=2
    elif (np.count_nonzero(row == 'CC')>0 and np.count_nonzero(row == 'CT')>0) or  (np.count_nonzero(row == 'CT')>0 and np.count_nonzero(row == 'TT')>0):
        ref = 2
        alt = 1
        for i in range(num_elem):
            if row[i] == "CC":
                row[i]=0
            elif row[i] == 'CT':
                row[i]=1
            elif row[i] == 'TT':
                row[i]=2
    elif (np.count_nonzero(row == 'CC')>0 and np.count_nonzero(row == 'CG')>0) or (np.count_nonzero(row == 'CG')>0 and np.count_nonzero(row == 'GG')>0):
        ref = 2
        alt = 3
        for i in range(num_elem):
            if row[i] == "CC":
                row[i]=0
            elif row[i] == 'CG':
                row[i]=1
            elif row[i] == 'GG':
                row[i]=2
      
    return [row.astype(int), ref, alt]
                
# function that performs the homomorphic encryption of the dataset after the entries have been mapped to integers
def he_data_row(data, public_key):
    """
    Function that returns the dataset encrypted.

    Parameters:
        data (ndarray): The dataset that we want to enctypt.

    Output: 
        cyphertexts (list): The dataset with each element mapped to integers.
    """
    cyphertexts = []
    for elem in data:
        gen = [public_key.encrypt(0),public_key.encrypt(0)]
        if elem == 1:
            gen[elem] = public_key.encrypt(1)
        elif elem == 0:
            gen[elem] = public_key.encrypt(2)
        
        cyphertexts.append(gen)
    return cyphertexts

def get_proof_request_obj(issuer_did, connection_id):
    print("Request proof of research status and affiliation from researcher")
    #Set some variables
    revocation = False
    SELF_ATTESTED = False
    exchange_tracing = False

    #Enable this to ask for attributes to identity a user
    req_attrs = [
        {"name": "Role", "restrictions": [{"issuer_did": issuer_did}]},
        {"name": "Affiliation", "restrictions": [{"issuer_did": issuer_did}]},
    ]
    
    if SELF_ATTESTED:
        # self-attested agreement to follow the ethical guidline regarding data use
        req_attrs.append({"name": "Ethical Agreement"},)

    #Set predicates for Zero Knowledge Proofs (not required for the test use-case)
    req_preds = {}

    indy_proof_request = {
        "name": "Proof of Personal Information",
        "version": "1.0",
        "requested_attributes": {
            f"0_{req_attr['name']}_uuid":
            req_attr for req_attr in req_attrs
        },
        "requested_predicates": req_preds,
    }

    proof_request_obj = {
        "connection_id": connection_id,
        "proof_request": indy_proof_request,
        "trace": exchange_tracing,
    }
    
    return proof_request_obj