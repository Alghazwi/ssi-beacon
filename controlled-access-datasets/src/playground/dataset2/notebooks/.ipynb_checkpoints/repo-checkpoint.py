import asyncio
from aries_cloudcontroller import AriesAgentController
import json
from termcolor import colored
from phe import paillier
import re
import os
import sys
import utils
import numpy as np
from termcolor import colored
from phe import paillier
import time
import pickle

class Repo:
    def __init__(self, agent_config):
        
        self.agent_controller = agent_controller = AriesAgentController(agent_config["admin_url"],agent_config["api_key"])
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.agent_controller.init_webhook_server(agent_config["webhook_host"], agent_config["webhook_port"]))
        whost = agent_config["webhook_host"]
        wport = agent_config["webhook_port"]
        print(f"Listening for webhooks from agent at http://{whost}:{wport}")
        self._register_agent_listeners()
        print("listeners registered")
        # all connections in the form {"sender":connection_id}
        self.connections_dict = {}
        self.credentials = []
        self.trusted_dids = []
        self.pk_file = None
        self.n = 100
        self.dataset_file = "./datasets/org1_100samples.txt"

    def _register_agent_listeners(self):
        listeners=[{
            "handler": self._pk_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._connection_handler,
            "topic": "connections"
        },
        {
            "handler": self._proof_handler,
            "topic": "present_proof"
        }]

        self.agent_controller.register_listeners(listeners, defaults=True)

    # create invite to send to receiver
    def invite(self,receiver):
        loop = asyncio.get_event_loop()
        alias = None
        auto_accept = False
        public = "false"
        multi_use = "false"
        invitation_response = loop.run_until_complete(self.agent_controller.connections.create_invitation(alias, auto_accept, public, multi_use))
        ##### connection id #####
        connection_id = invitation_response["connection_id"]
        self.connections_dict[receiver] = connection_id
        invitation = invitation_response["invitation"]
        #### print invitation ####
        print(f'Copy the invitation and paste it to {receiver} notebook')
        print(invitation)

    # connect with other agents given the sender name
    def connect_with(self,invitation, sender):
        #options
        auto_accept="false"
        alias = sender
        loop = asyncio.get_event_loop()
        # receive connection
        invite_response = loop.run_until_complete(self.agent_controller.connections.receive_invitation(invitation, alias, auto_accept))
        connection_id = invite_response["connection_id"]
        # save connection_id to dict
        self.connections_dict[sender] = connection_id
        # accept the connection
        # options
        label = alias
        endpoint = None
        accept_response = loop.run_until_complete(self.agent_controller.connections.accept_invitation(connection_id, label, endpoint))
 

    def send_enc_data(self):
        import time
        n = self.n
        receiver = "maf"
        loop = asyncio.get_event_loop()
        enc_data_file = f"./enc_datasets/phe_org1_{n}samples.txt"
        print(enc_data_file)
        f = open(enc_data_file,"r")
        data = f.read()
        data_json = json.loads(data)
        values = data_json["values"]
        print(len(values))
        for i in range(len(values)):
            send_data = {"type":"phe"}
            send_data["data"] = {"index": i, "value": values[i]}
            send_data_final = json.dumps(send_data) 
            content = str(send_data_final)
            #print(content)
            response = loop.run_until_complete( self.agent_controller.messaging.send_message(self.connections_dict[receiver], content))
            time.sleep(0.5)
            
        f.close()
        print("Encrypted dataset sent.")
    
    # send ack
    def send_ack(self):
        loop = asyncio.get_event_loop()
        ack = {"type":"ack"}
        receiver = "maf"
        send_data_final = json.dumps(ack) 
        content = str(send_data_final)
            #print(content)
        response = loop.run_until_complete( self.agent_controller.messaging.send_message(self.connections_dict[receiver], content))
        
    # HE Helper functions


    def encode_encrypt(self):

        time_start = time.time()
        
        # get the public key from the file pk_file
        pk_file = "publickey.txt"
        file = open(pk_file)
        line = file.read().replace("\n", " ")
        file.close()

        received_dict = json.loads(line)
        pk = received_dict['public_key']
        public_key = paillier.PaillierPublicKey(n=int(pk['n']))

        # names of the input and output files
        data_file = self.dataset_file
        n = self.n
        enc_data_file = f"./enc_datasets/phe_org1_{n}samples.txt"
        rows = 1

        # read the dataset in dataset_file with n number of elements as defined earlier 
        data = utils.get_data(data_file,num_elem=n)

        # map the two dataset entries to integers
        map_c, ref, alt = utils.map_data(data,num_elem=n,row_num=1)

        # save the ref and alt
        file = open('ref_alt.txt', 'wb')
        pickle.dump([ref,alt], file)
        file.close()

        # timing
        time_end = time.time()
        print('Execution time encoding dataset 1 = {} s'.format(time_end-time_start))

        time_start = time.time()

        # encrypt the dataset for the number of rows indicated 
        he_data = utils.he_data_row(map_c, public_key)
        he_data = np.asarray(he_data)
        he_data_flat = he_data.ravel()

        enc_with_one_pub_key = {}
        enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in he_data_flat]
        serialised2 = json.dumps(enc_with_one_pub_key)
        text_file = open(enc_data_file, "w")
        text_file.write(serialised2)
        text_file.close()
        print("Enctypted dataset saved it file {}\n".format(enc_data_file))

        # timing
        time_end = time.time()
        print('Execution time encrypting dataset 1 = {} s'.format(time_end-time_start))

    def get_data(self,path,num_elem):

        """
        Function to open the .txt files of the datasets and put the data into a numpy array with 101 columns.

        Parameters:
            path (str): The path + file name of the dataset.

        Output: 
            data5 (ndarray): The dataset as a numpy array.
        """
        f = open(path, "r")
        data = f.read()
        data2 = re.split(' |\n|\n ',data)
        data3 = data2[:-1]
        data3.insert(0, 'name')
        data4=np.asarray(data3)
        data5=data4.reshape(312, num_elem+1) #31512 elements in total for both datasets
        return data5


    def map_data(self,data,num_elem,row_num):
        """
        Function that returns the data mapped to integres (100 is the number of samples in each row - should be changed if the dataset is different)

        Parameters:
            data (ndarray): The dataset that we want to map to integers.
            num_elem (int): Number of individuals used in the dataset (so the number of samples).

        Output: 
            m_data (ndarray): The dataset with each element mapped to integers.
        """
        m_data = data[1:,1:].copy()
        row=m_data[row_num] # take the row we want

        if (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AG')>0) or (np.count_nonzero(row == 'AG')>0 and np.count_nonzero(row == 'GG')>0):
            ref = 0 # the corresponding ref and alt indicated with an integer
            alt = 3
            for i in range(num_elem):
                if row[i] == "AA":
                    row[i]=0
                elif row[i] == 'AG':
                    row[i]=1
                elif row[i] == 'GG':
                    row[i]=2
        elif (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AT')>0) or (np.count_nonzero(row == 'AT')>0 and np.count_nonzero(row == 'TT')>0):
            ref = 0
            alt = 1
            for i in range(num_elem):
                if row[i] == "AA":
                    row[i]=0
                elif row[i] == 'AT':
                    row[i]=1
                elif row[i] == 'TT':
                    row[i]=2
        elif (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AC')>0) or (np.count_nonzero(row == 'AC')>0 and np.count_nonzero(row == 'CC')>0):
            ref = 0
            alt = 2
            for i in range(num_elem):
                if row[i] == "AA":
                    row[i]=0
                elif row[i] == 'AC':
                    row[i]=1
                elif row[i] == 'CC':
                    row[i]=2
        elif (np.count_nonzero(row == 'GG')>0 and np.count_nonzero(row == 'GT')>0) or (np.count_nonzero(row == 'GT')>0 and np.count_nonzero(row == 'TT')>0):
            ref = 3
            alt = 1
            for i in range(num_elem):
                if row[i] == "GG":
                    row[i]=0
                elif row[i] == 'GT':
                    row[i]=1
                elif row[i] == 'TT':
                    row[i]=2
        elif (np.count_nonzero(row == 'CC')>0 and np.count_nonzero(row == 'CT')>0) or  (np.count_nonzero(row == 'CT')>0 and np.count_nonzero(row == 'TT')>0):
            ref = 2
            alt = 1
            for i in range(num_elem):
                if row[i] == "CC":
                    row[i]=0
                elif row[i] == 'CT':
                    row[i]=1
                elif row[i] == 'TT':
                    row[i]=2
        elif (np.count_nonzero(row == 'CC')>0 and np.count_nonzero(row == 'CG')>0) or (np.count_nonzero(row == 'CG')>0 and np.count_nonzero(row == 'GG')>0):
            ref = 2
            alt = 3
            for i in range(num_elem):
                if row[i] == "CC":
                    row[i]=0
                elif row[i] == 'CG':
                    row[i]=1
                elif row[i] == 'GG':
                    row[i]=2

        return [row.astype(int), ref, alt]

    # function that performs the homomorphic encryption of the dataset after the entries have been mapped to integers
    def he_data_row(self,data, public_key):
        """
        Function that returns the dataset encrypted.

        Parameters:
            data (ndarray): The dataset that we want to enctypt.

        Output: 
            cyphertexts (list): The dataset with each element mapped to integers.
        """
        cyphertexts = []
        for elem in data:
            gen = [public_key.encrypt(0),public_key.encrypt(0)]
            if elem == 1:
                gen[elem] = public_key.encrypt(1)
            elif elem == 0:
                gen[elem] = public_key.encrypt(2)

            cyphertexts.append(gen)
        return cyphertexts

    ##############################################################
    ##############################################################
    # 
    # Listeners
    # 
    # ##############################################################
    # ##############################################################    

    # Receive connection messages
    def _connection_handler(self, payload):
        loop = asyncio.get_event_loop()
        state = payload['state']
        connection_id = payload["connection_id"]
        their_role = payload["their_role"]
        routing_state = payload["routing_state"]

        print("----------------------------------------------------------")
        print("Connection Webhook Event Received")
        print("Connection ID : ", connection_id)
        print("State : ", state)
        print("Routing State : ", routing_state)
        print("Their Role : ", their_role)
        print("----------------------------------------------------------")

        if state == "invitation":
            print("invited")
        if state == "request":
            print("Accepting request ", connection_id)
        elif state == "response":
            print("responded")
        elif state == "active":
            time.sleep(2)
            print("Sending trust ping", connection_id)
            loop.run_until_complete(self.agent_controller.messaging.trust_ping(connection_id, "hello"))
            print(colored("Connection ID: {0} is now active.".format(
                connection_id), "green", attrs=["bold"]))
    
    # handle pk messages
    def _pk_messages_handler(self, payload):
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json['type'] == 'public_key':
                print("Public Key Message Handler", payload, connection_id)
                pk_file = "publickey.txt"
                pk = content_json["data"]
                try:
                    f = open(pk_file, "w+")
                    json.dump(pk, f)
                    f.close()
                    print("public key saved to file: publickey.txt")
                    self.pk_file = pk_file
                    self.encode_encrypt()
                    self.send_enc_data()
                    self.send_ack()
                except Exception as e:
                    print("Error writing file", e)
                    return
        except Exception as e:
            print("message not handeled by pk message handler")
            return

    def _issuer_handler(self, payload):
        connection_id = payload['connection_id']
        exchange_id = payload['credential_exchange_id']
        state = payload['state']
        role = payload['role']
        print("\n---------------------------------------------------\n")
        print("Handle Issue Credential Webhook")
        print(f"Connection ID : {connection_id}")
        print(f"Credential exchange ID : {exchange_id}")
        print("Agent Protocol Role : ", role)
        print("Protocol State : ", state )
        print("\n---------------------------------------------------\n")
        if state == "offer_sent":
            proposal = payload["credential_proposal_dict"]
            attributes = proposal['credential_proposal']['attributes']
            print(f"Offering credential with attributes  : {attributes}")
            ## YOUR LOGIC HERE
        elif state == "request_received":
            print("Request for credential received")
            ## YOUR LOGIC HERE
        elif state == "credential_sent":
            print("Credential Sent")
            ## YOUR LOGIC HERE
            
    def _proof_handler(self, payload):
        role = payload["role"]
        connection_id = payload["connection_id"]
        pres_ex_id = payload["presentation_exchange_id"]
        state = payload["state"]
        print("\n---------------------------------------------------------------------\n")
        print("Handle present-proof")
        print("Connection ID : ", connection_id)
        print("Presentation Exchange ID : ", pres_ex_id)
        print("Protocol State : ", state)
        print("Agent Role : ", role)
        print("Initiator : ", payload["initiator"])
        print("\n---------------------------------------------------------------------\n")
        if state == "request_sent":
            print("Presentation Request\n")
            print(payload["presentation_request"])
            print("\nThe presentation request is encoded in base64 and packaged into a DIDComm Message\n")
            print(payload["presentation_request_dict"])
            print("\nNote the type defines the protocol present-proof and the message request-presentation\n")
            print(payload["presentation_request_dict"]["@type"])
        elif state == "presentation_received":
            print("Presentation Received")
            print("We will not go into detail on this payload as it is comparable to the presentation_sent we looked at in the earlier cell.")
            print("This is the full payload\n")
            print(payload)
        else:
            print("Paload \n")
            print(payload)
