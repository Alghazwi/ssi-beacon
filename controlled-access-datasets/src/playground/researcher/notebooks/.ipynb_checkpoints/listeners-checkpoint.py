# Listeners functions

import json
from termcolor import colored
from phe import paillier

# Receive connection messages
def _connection_handler(payload):
    state = payload['state']
    connection_id = payload["connection_id"]
    their_role = payload["their_role"]
    routing_state = payload["routing_state"]

    print("----------------------------------------------------------")
    print("Connection Webhook Event Received")
    print("Connection ID : ", connection_id)
    print("State : ", state)
    print("Routing State : ", routing_state)
    print("Their Role : ", their_role)
    print("----------------------------------------------------------")

    if state == "invitation":
        # Your business logic
        print("invitation")
    elif state == "request":
        # Your business logic
        print("request")

    elif state == "response":
        # Your business logic
        print("response")
    elif state == "active":
        # Your business logic
        print(colored("Connection ID: {0} is now active.".format(
            connection_id), "green", attrs=["bold"]))

# handle pk messages
def _result_messages_handler(payload):
    connection_id = payload["connection_id"]
    content = payload["content"]
    try:
        content_json = json.loads(content)
        if content_json['type'] == 'result':
            print("Result Message Handler", payload, connection_id)
            #[TODO] change file name to include connection_id
            result_file = "result.txt"
            result = content_json["data"]
            try:
                f = open(result_file, "w+")
                json.dump(result, f)
                f.close()
                #[TODO] change this as well
                print("result saved to file: result.txt")
            except Exception as e:
                print("Error writing file", e)
                return
    except Exception as e:
        print("message not handeled by results message handler")
        return

# handle credential messages
def _credential_handler(payload):
    connection_id = payload['connection_id']
    exchange_id = payload['credential_exchange_id']
    state = payload['state']
    role = payload['role']
    print("\n---------------------------------------------------\n")
    print("Handle Issue Credential Webhook")
    print(f"Connection ID : {connection_id}")
    print(f"Credential exchange ID : {exchange_id}")
    print("Agent Protocol Role : ", role)
    print("Protocol State : ", state)
    print("\n---------------------------------------------------\n")
    print("Handle Credential Webhook Payload")

    if state == "offer_received":
        proposal = payload["credential_proposal_dict"]
        print("The proposal dictionary is likely how you would understand and display a credential offer in your application")
        print("\n", proposal)
        print("\n This includes the set of attributes you are being offered")
        attributes = proposal['credential_proposal']['attributes']
        print(attributes)
        # YOUR LOGIC HERE
    elif state == "request_sent":
        print("\nA credential request object contains the commitment to the agents master secret using the nonce from the offer")
        # YOUR LOGIC HERE
    elif state == "credential_received":
        print("Received Credential")
        # YOUR LOGIC HERE
    elif state == "credential_acked":
        # YOUR LOGIC HERE
        credential = payload["credential"]
        print("Credential Stored\n")
        print(credential)

        print(
            "\nThe referent acts as the identifier for retrieving the raw credential from the wallet")
        # Note: You would probably save this in your application database
        credential_referent = credential["referent"]
        print("Referent", credential_referent)

# handle proof messages
def _proof_handler(payload):
    role = payload["role"]
    connection_id = payload["connection_id"]
    pres_ex_id = payload["presentation_exchange_id"]
    state = payload["state"]
    print("\n---------------------------------------------------------------------\n")
    print("Handle present-proof")
    print("Connection ID : ", connection_id)
    print("Presentation Exchange ID : ", pres_ex_id)
    print("Protocol State : ", state)
    print("Agent Role : ", role)
    print("Initiator : ", payload["initiator"])
    print("\n---------------------------------------------------------------------\n")

    if state == "request_received":
        presentation_request = payload["presentation_request"]
        print("Recieved Presentation Request\n")
        print("\nRequested Attributes - Note the restrictions. These limit the credentials we could respond with\n")
        print(presentation_request["requested_attributes"])
    elif state == "presentation_sent":
        print("Presentation sent\n")

    elif state == "presentation_acked":
        print("Presentation has been acknowledged by the Issuer")
