# Helper functions 

import json
from termcolor import colored
from phe import paillier

# Create public key to use for the encryption
def create_pk():
    # generate the public and secret key pair
    public_key, secret_key = paillier.generate_paillier_keypair() 

    # saving the public key in a text file 
    enc_with_one_pub_key = {}
    enc_with_one_pub_key['public_key'] = {'n': public_key.n}
    serialised = json.dumps(enc_with_one_pub_key)
    text_file = open("publickey.txt", "w")
    text_file.write(serialised)
    text_file.close()

    # saving the secret key in a text file 
    enc_with_one_sec_key = {}
    enc_with_one_sec_key['secret_key'] = {'p': secret_key.p, 'q': secret_key.q }
    serialised = json.dumps(enc_with_one_sec_key)
    text_file = open("secretkey.txt", "w")
    text_file.write(serialised)
    text_file.close()

    #print
    response = "key pair created and saved to files: publickey.txt, privatekey.txt"
    print(response)

def send_pk(pk_file):
    send_data_final = None
    try:
        f = open(pk_file, "r")
        print("public key file opened")
        pk = f.read()
        pk_json = json.loads(pk)
        send_data = {"type":"public_key"}
        send_data["data"] = pk_json
        send_data_final = json.dumps(send_data)    
        f.close()
    except:
        print("Unable to open file")
    print(send_data_final)
    return send_data_final

def create_query(pk_file):
    send_data_final = None
    try:
        f = open(pk_file, "r")
        print("public key file opened")
        pk = f.read()
        pk_json = json.loads(pk)
        send_data = {"type":"query"}
        send_data["q"] = "MAF"
        send_data["pk_data"] = pk_json
        send_data_final = json.dumps(send_data)    
        f.close()
    except:
        print("Unable to open file")
    print(send_data_final)
    return send_data_final
    
# function to decrypt the allele counts and output the MAF
def decryption_and_MAF(al_count, secret_key, ref_alt, num_elem):
    """
    Function to decrypt the allele count and return the final MAF result.

    Parameters:
        allele_counts (ndarray): The encrypted allele counts using the combined datasets.

    Output: 
        result (str): The minimum allele frequency written as a fraction over the total number of alleles.
    """ 
    
    a = [secret_key.decrypt(x) for x in al_count][0]

    ref_alt_str = []
    for i in range(len(ref_alt)):
        if ref_alt[i] == 0:
            ref_alt_str.append('A')
        elif ref_alt[i] == 1:
            ref_alt_str.append('T')
        elif ref_alt[i] == 2:
            ref_alt_str.append('C')
        elif ref_alt[i] == 3:
            ref_alt_str.append('G')
    
    if a<(num_elem*4-a):
        maf = f"The MAF is {a}/{num_elem*4} for allele {ref_alt_str[0]}."
    elif a>(num_elem*4-a):
        maf = f"The MAF is {num_elem*4-a}/{num_elem*4} for allele {ref_alt_str[1]}."
    else:
        maf = f"There are exactly the same amount of {ref_alt_str[0]} and {ref_alt_str[1]}."

    return maf

def createTask(self, connection_id, size ):
    print("Creating a task ...")
    # creating a task id
    task_id = uuid.uuid4()
    self.tasks[str(task_id)] = {}
    # adding connection to this task
    self.tasks[str(task_id)]["connection_id"] = connection_id
    self.tasks[str(task_id)]["size"] = size
    #create file for the task data
    file_name = "phe_" + task_id + ".txt"
    f = open(file_name,"w+")
    line = '{"enc_type":"phe",' + '"size":'+size+', "data":'
    f.write(line)
    f.close()

    return task_id

def create_presentation(credentials, presentation_record):
    # select credentials to provide for the proof
    #credentials = await agent_controller.proofs.get_presentation_credentials(presentation_exchange_id)
    #print("Credentials stored that could be used to satisfy the request. In some situations you applications may have a choice which credential to reveal\n")

    attribute_by_reft = {}
    revealed = {}
    self_attested = {}
    predicates = {}


    if credentials:
        for credential in credentials:

            for attribute_reft in credential["presentation_referents"]:
                if attribute_reft not in attribute_by_reft:
                    attribute_by_reft[attribute_reft] = credential

    for (key, value) in attribute_by_reft.items():
        print(f"Attribute {presentation_record['presentation_request']['requested_attributes'][key]} can be satisfied by Credential with Referent -- {value['cred_info']['referent']}")
                    
    for attribute_reft in presentation_record["presentation_request"]["requested_attributes"]:
        if attribute_reft in attribute_by_reft:
            revealed[attribute_reft] = {
                "cred_id": attribute_by_reft[attribute_reft]["cred_info"][
                    "referent"
                ],
                "revealed": True,
            }


    print("\nGenerate the proof")
    presentation = {
        "requested_predicates": predicates,
        "requested_attributes": revealed,
        "self_attested_attributes": self_attested,
    }
    print(presentation)
    return presentation


