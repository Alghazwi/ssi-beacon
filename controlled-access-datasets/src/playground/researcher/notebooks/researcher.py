import asyncio
from aries_cloudcontroller import AriesAgentController
import json
import os
import sys
import numpy as np
import time
from termcolor import colored
from phe import paillier
import pickle
from libs.utils.he_utils import *

class Researcher:
    
    def __init__(self, agent_config):
        
        self.agent_controller = agent_controller = AriesAgentController(agent_config["admin_url"],agent_config["api_key"])
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.agent_controller.init_webhook_server(agent_config["webhook_host"], agent_config["webhook_port"]))
        whost = agent_config["webhook_host"]
        wport = agent_config["webhook_port"]
        print(f"Listening for webhooks from agent at http://{whost}:{wport}")
        self._register_agent_listeners()
        print("listeners registered")
        self.credentials = []
        # all connections in the form {"sender":connection_id}
        self.connections_dict = {}
        # keys
        self.key_dir = "./he_data/keys/"
        self.pk_file = None
        self.sk_file = None
        # query data
        self.q = None
        self.n = None
        # result
        self.result_dir = "./he_data/result/"
        self.result_file = None
        self.ref_alt = "./he_data/ref_alt.txt"
        # performance 
        self.max = 10
        self.time_start = None
        self.time_end = None
        


    # register all relivant listener for this agent    
    def _register_agent_listeners(self):
        
        
        l=[{
            "handler": self._result_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._connection_handler,
            "topic": "connections"
        },
        {
            "handler": self._credential_handler,
            "topic": "issue_credential"
        },
        {
            "handler": self._proof_handler,
            "topic": "present_proof"
        }]

        self.agent_controller.register_listeners(l)
        
    ##############################################################
    ##############################################################
    # 
    # Listeners
    # 
    # ##############################################################
    # ##############################################################       

    # Receive connection messages
    def _connection_handler(self, payload):
        loop = asyncio.get_event_loop()
        state = payload['state']
        connection_id = payload["connection_id"]
        their_role = payload["their_role"]
        routing_state = payload["routing_state"]

        print("----------------------------------------------------------")
        print("Connection Webhook Event Received")
        print("Connection ID : ", connection_id)
        print("State : ", state)
        print("Routing State : ", routing_state)
        print("Their Role : ", their_role)
        print("----------------------------------------------------------")
        
        if state == "active":
            time.sleep(2)
            print("Sending trust ping", connection_id)
            loop.run_until_complete(self.agent_controller.messaging.trust_ping(connection_id, "hello"))
            print(colored("Connection ID: {0} is now active.".format(
                connection_id), "green", attrs=["bold"]))

    # handle result messages
    def _result_messages_handler(self, payload):
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json['type'] == 'result':
                print("Result Message Handler", payload, connection_id)
                result_file = self.result_dir + f'result_{connection_id}.txt'
                self.result_file = result_file
                result = content_json["data"]
                self.n = content_json["n"]
                try:
                    f = open(result_file, "w+")
                    json.dump(result, f)
                    f.close()
                    print("result saved")
                    self.result_file = result_file
                    decrypt(self.pk_file, self.sk_file, self.ref_alt, self.result_file, self.n)
                except Exception as e:
                    print("Error writing file", e)
                    return
        except Exception as e:
            #print("message not handeled by results message handler")
            return
        

    # handle credential messages
    def _credential_handler(self, payload):
        loop = asyncio.get_event_loop()
        connection_id = payload['connection_id']
        exchange_id = payload['credential_exchange_id']
        state = payload['state']
        role = payload['role']
        print("\n---------------------------------------------------\n")
        print("Handle Issue Credential Webhook")
        print(f"Connection ID : {connection_id}")
        print(f"Credential exchange ID : {exchange_id}")
        print("Agent Protocol Role : ", role)
        print("Protocol State : ", state)
        print("\n---------------------------------------------------\n")
        print("Handle Credential Webhook Payload")

        if state == "offer_received":
            proposal = payload["credential_proposal_dict"]
            attributes = proposal['credential_proposal']['attributes']
            print(f"Offering: {attributes}")
            print("Sending request for credential")
            loop.run_until_complete(self.agent_controller.issuer.send_request_for_record(exchange_id))
        elif state == "request_sent":
            print("\n Request Sent")
        elif state == "credential_received":
            print("Storing Credential ...")
            response = loop.run_until_complete(self.agent_controller.issuer.store_credential(exchange_id, "Researcher Credential")) 
            state = response['state']
            role = response['role']
            print(f"Credential exchange {exchange_id}, role: {role}, state: {state}")

        elif state == "credential_acked":
            credential = payload["credential"]
            print("Credential Stored\n")
            print(credential)
            credential_referent = credential["referent"]
            self.credentials.append(credential_referent)
            print("Referent", credential_referent)

    # handle proof messages
    def _proof_handler(self, payload):
        loop = asyncio.get_event_loop()
        role = payload["role"]
        connection_id = payload["connection_id"]
        pres_ex_id = payload["presentation_exchange_id"]
        state = payload["state"]
        print("\n---------------------------------------------------------------------\n")
        print("Handle present-proof")
        print("Connection ID : ", connection_id)
        print("Presentation Exchange ID : ", pres_ex_id)
        print("Protocol State : ", state)
        print("Agent Role : ", role)
        print("Initiator : ", payload["initiator"])
        print("\n---------------------------------------------------------------------\n")

        if state == "request_received":
            presentation_request = payload["presentation_request"]
            print("Recieved Presentation Request\n")
            print("\nRequested Attributes - Note the restrictions. These limit the credentials we could respond with\n")
            print(presentation_request["requested_attributes"])
            # Query parameters
            verifier_connection_id = connection_id
            thread_id=None
            state = "request_received"
            role = "prover"
            proof_records_response = loop.run_until_complete( self.agent_controller.proofs.get_records(verifier_connection_id, thread_id, state, role))
            # Get the first record from the response.
            presentation_record = proof_records_response["results"][0]
            # select credentials to provide for the proof
            cred = loop.run_until_complete( self.agent_controller.proofs.get_presentation_credentials(pres_ex_id))
            # create presentation
            presentation = self.create_presentation(cred, presentation_record)
            # send presentation
            presentation_response = loop.run_until_complete( self.agent_controller.proofs.send_presentation(pres_ex_id, presentation))

        elif state == "presentation_sent":
            print("Presentation sent\n")

        elif state == "presentation_acked":
            print("Presentation has been acknowledged by the Issuer")

    ##############################################################
    ##############################################################
    # 
    # additional functions
    # 
    # ##############################################################
    # ##############################################################  

    # connect with other agents given the initation
    def connect_with(self,invitation, sender):
        #options
        auto_accept="false"
        alias = sender
        loop = asyncio.get_event_loop()
        # receive connection
        invite_response = loop.run_until_complete(self.agent_controller.connections.receive_invitation(invitation, alias, auto_accept))
        connection_id = invite_response["connection_id"]
        # save connection_id to dict
        self.connections_dict[sender] = connection_id
        # accept the connection
        # options
        label = sender
        endpoint = None
        accept_response = loop.run_until_complete(self.agent_controller.connections.accept_invitation(connection_id, label, endpoint))
 
     # create invite to send to receiver
    def invite(self,receiver):
        loop = asyncio.get_event_loop()
        alias = None
        auto_accept = False
        public = "false"
        multi_use = "false"
        invitation_response = loop.run_until_complete(self.agent_controller.connections.create_invitation(alias, auto_accept, public, multi_use))
        ##### connection id #####
        connection_id = invitation_response["connection_id"]
        self.connections_dict[receiver] = connection_id
        invitation = invitation_response["invitation"]
        #### print invitation ####
        print(f'Copy the invitation and paste it to {receiver} notebook')
        print(invitation)
        
    # create a query message to send
    def create_query_message(self,query, n, pk_file):
        send_data_final = None
        try:
            f = open(pk_file, "r")
            print("public key file opened")
            pk = f.read()
            pk_json = json.loads(pk)
            send_data = {"type":"query_message"}
            send_data["query"] = query
            send_data["n"] = n
            send_data["public_key"] = pk_json
            send_data_final = json.dumps(send_data)    
            f.close()
        except:
            print("Unable to open file")
        print(send_data_final)
        return send_data_final

    # send query to receiver
    def send_query(self,query,receiver):
        loop = asyncio.get_event_loop()
        response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict[receiver], query))
        print(f'Query sent to {receiver}')
    
    # create presentation for proof verfication
    def create_presentation(self, credentials, presentation_record):
        attribute_by_reft = {}
        revealed = {}
        self_attested = {}
        predicates = {}

        if credentials:
            for credential in credentials:

                for attribute_reft in credential["presentation_referents"]:
                    if attribute_reft not in attribute_by_reft:
                        attribute_by_reft[attribute_reft] = credential

        for (key, value) in attribute_by_reft.items():
            print(f"Attribute {presentation_record['presentation_request']['requested_attributes'][key]} can be satisfied by Credential with Referent -- {value['cred_info']['referent']}")
                        
        for attribute_reft in presentation_record["presentation_request"]["requested_attributes"]:
            if attribute_reft in attribute_by_reft:
                revealed[attribute_reft] = {
                    "cred_id": attribute_by_reft[attribute_reft]["cred_info"][
                        "referent"
                    ],
                    "revealed": True,
                }


        #print("\nGenerate the proof")
        presentation = {
            "requested_predicates": predicates,
            "requested_attributes": revealed,
            "self_attested_attributes": self_attested,
        }
        #print(presentation)
        return presentation
