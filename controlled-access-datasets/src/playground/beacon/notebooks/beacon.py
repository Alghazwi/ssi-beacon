import asyncio
from aries_cloudcontroller import AriesAgentController
import json
import os
import sys
import numpy as np # linear algebra
import time
from libs.utils.he_utils import *
from termcolor import colored
from phe import paillier
from libs.authentication_service import AuthenticationService


class Beacon:
    
    def __init__(self, agent_config):
        # initialize controller
        self.agent_controller = agent_controller = AriesAgentController(agent_config["admin_url"],agent_config["api_key"])
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.agent_controller.init_webhook_server(agent_config["webhook_host"], agent_config["webhook_port"]))
        whost = agent_config["webhook_host"]
        wport = agent_config["webhook_port"]
        print(f"Listening for webhooks from agent at http://{whost}:{wport}")
        # register listeners
        self._register_agent_listeners()
        print("listeners registered")
        # auth service
        self.auth_service = AuthenticationService(self.agent_controller)
        # stored credentials
        self.credentials = []
        # public key
        self.pk_dir = "./he_data/pk/"
        self.pk_file = None
        # Encrypted files
        self.phe_dir = "./he_data/enc_dataset/"
        self.phe_files = []
        self.phe_file_counts = 0
        # Results file
        self.result_dir = "./he_data/result/"
        self.result_file = None
        # N = number of records
        self.n = None
        

    # register all relivant listener for this agent    
    def _register_agent_listeners(self):
        
        
        l=[{
            "handler": self._phe_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._query_messages_handler,
            "topic": "basicmessages"
        }]

        self.agent_controller.register_listeners(l)

    ##############################################################
    ##############################################################
    # 
    # Matching algorithm
    # 
    # ##############################################################
    # ##############################################################  

    def matching_algorithm(self, connection_id, query):
        pass
        # check attributes of researcher through connection id 

        # loop through all datasets to match consent with purpose
        # at the same time look at metadata to see if query/genotype exists
        # store all connections of matching datasets in an array

        # send invite to participate to all found matches
    
    ##############################################################
    ##############################################################
    # 
    # Listeners
    # 
    # ##############################################################
    # ##############################################################       

    # handle query messages
    def _query_messages_handler(self,payload):
        loop = asyncio.get_event_loop()
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if self.auth_service.connection_trusted(connection_id) and content_json['type'] == 'query_message':
                print("Query Message Handler", payload, connection_id)
                query = content_json["query"]
                pk_file = self.pk_dir + f'publickey_{connection_id}.txt'
                self.n = content_json["n"]
                pk = content_json["public_key"]
                try:
                    f = open(pk_file, "w+")
                    json.dump(pk, f)
                    f.close()
                    print("public key saved")
                    self.pk_file = pk_file
                    #send invite to datasets
                    self.sendInvites(content)
                except Exception as e:
                    print("Error writing file", e)
                    return
        except Exception as e:
            #print("message not handeled by query message handler")
            return
        

    # listener to recieve phe data 
    def _phe_messages_handler(self,payload):
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if self.auth_service.connection_trusted(connection_id) and content_json["type"] == "phe":
                print("PHE encrypted data Message Handler", payload, connection_id)
                phe_file = self.phe_dir + f'phe_{connection_id}.txt'
                phe_data = content_json["data"]
                try:
                    f = open(phe_file, "w")
                    d = json.dumps(content_json)
                    f.write(d)
                    f.close()
                    self.phe_file_counts += 1
                    self.phe_files.append(phe_file)
                    if self.phe_file_counts == 2:
                        self.do_maf()
                        self.send_result()
                except Exception as e:
                    print("Error writing file", e)
                    return
        except Exception as e:
            #print("not handeled by phe_handler")
            return

    #############################
    # additional Functions
    #############################

    def sendInvites(self, content):
        loop = asyncio.get_event_loop()
        for con in self.auth_service.dataset_connections:
            if self.auth_service.connection_trusted(con):
                connection_id = con
                response = loop.run_until_complete(self.agent_controller.messaging.send_message(connection_id, content))
        print(f'invite sent to trusted dataset agents')

    # perform maf calculation
    def do_maf (self):
        dataset1_file = self.phe_files[0]
        dataset2_file = self.phe_files[1]
        con_id = self.auth_service.researcher_connections[0]
        self.result_file =  self.result_dir + f'result_{con_id}.txt'
        output_file = self.result_file
        maf(self.pk_file, dataset1_file, dataset2_file, output_file, self.n)
    
    # send result of query to researcher
    def send_result(self):
        loop = asyncio.get_event_loop()
        connection_id = self.auth_service.researcher_connections[0]
        content = self.create_result_message()
        response = loop.run_until_complete(self.agent_controller.messaging.send_message(connection_id, content))


    # Create result message to send
    def create_result_message(self):
        result_file = self.result_file
        send_data_final = None
        try:
            f = open(result_file, "r")
            print("results file opened")
            result = f.read()
            result_json = json.loads(result)
            send_data = {"type":"result"}
            send_data["n"] = self.n
            send_data["data"] = result_json
            send_data_final = json.dumps(send_data)    
            f.close()
        except:
            print("Unable to open file")
        print(send_data_final)
        return send_data_final