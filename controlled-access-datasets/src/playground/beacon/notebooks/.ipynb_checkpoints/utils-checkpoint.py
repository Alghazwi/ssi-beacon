# helper functions

import time
from phe import paillier
import numpy as np
import json
import sys
import re

def he_genotype_count(cyphertexts):

    """
    Function to count the relative genotypic count.

    Parameters:
        cyphertexts (ndarray): The relative genotypic count.

    Output: 
        sum_list (list): The relative genotypic count.
    """

    sum_list = []
    for i in range(2):
        summ=0
        for x in cyphertexts:
            summ+=x[i]
        sum_list.append(summ)
    return sum_list


def he_allel_count(freq):

    """
    Function to count the number of alleles.

    Parameters:
        freq (ndarray): The relative genotypic count, obtained from the output of the function he_genotype_count().

    Output: 
        allel_count (int): The count of one of the alleles.
    """

    allel_count = freq[0]+freq[1]
    return allel_count


def AF(row_num, data1, data2):

    """
    Function to calculate the allel counts incorporating all other functions.

    Parameters:
        row_num (int): The number of rows from the datasets that we want to calculate the allele frequency for.
        data1 (ndarray): The first encrypted dataset.
        data2 (ndarray): The second encrypted dataset.

    Output: 
        allel_count (list): The count of each of the alleles.
    """

    # merge the two rows for this same genotype
    merged = data1[row_num] + data2[row_num]
    
    # calculate the relative genotypic count
    gen_count = he_genotype_count(merged)
    
    # calculate the allel counts
    result = he_allel_count(gen_count)

    return result

# opening the encrypted datasets in the correct order
def opening_datasets_in_order(dataset_name, public_key_rec):
    file = open(dataset_name)
    data = file.read().replace("}{", "} , {") # adding a place to split the lines in the next step
    data2 = re.split(' , ',data) # split the lines

    # make a dict of dicts
    d = {}
    for elem in data2:
        x = json.loads(elem)
        d[x["index"]] = x["value"] # set the index value as the key of the subdict
    
    # sorting the dict of dicts according to the index value
    sorted_d = sorted(d.items(), key=lambda x: x[0], reverse=False)
    
    # make a dict from the sorted dict to match the form of the data before
    d_final = {}
    d_final["values"] = []
    for elem in sorted_d:
        d_final["values"].append(elem[1])
    
    case = [paillier.EncryptedNumber(public_key_rec, int(x[0]), int(x[1])) for x in d_final['values']]
    return case

# Create public key to use for the encryption
def create_result_message(result_file):
    send_data_final = None
    try:
        f = open(result_file, "r")
        print("results file opened")
        result = f.read()
        result_json = json.loads(result)
        send_data = {"type":"result"}
        send_data["data"] = result_json
        send_data_final = json.dumps(send_data)    
        f.close()
    except:
        print("Unable to open file")
    print(send_data_final)
    return send_data_final