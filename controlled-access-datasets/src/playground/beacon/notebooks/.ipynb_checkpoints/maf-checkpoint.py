import asyncio
from aries_cloudcontroller import AriesAgentController
import json
import os
import sys
import numpy as np # linear algebra
import time
import utils
from termcolor import colored
from phe import paillier


class MAF:
    
    def __init__(self, agent_config):
        
        self.agent_controller = agent_controller = AriesAgentController(agent_config["admin_url"],agent_config["api_key"])
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.agent_controller.init_webhook_server(agent_config["webhook_host"], agent_config["webhook_port"]))
        whost = agent_config["webhook_host"]
        wport = agent_config["webhook_port"]
        print(f"Listening for webhooks from agent at http://{whost}:{wport}")
        self._register_agent_listeners()
        print("listeners registered")
        # all researcher connections in the form {"sender":connection_id}
        self.connections_dict = {}
        # all dataset agents connections in the form {"dataset":connection_id} 
        self.connections_datasets = {}
        self.ack = [0,0]
        self.credentials = []
        self.pk_file = None
        self.pk = None
        self.phe_files = {}
        self.result_file = None
        self.n = 100


    # register all relivant listener for this agent    
    def _register_agent_listeners(self):
        
        
        l=[{
            "handler": self._phe_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._ack_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._pk_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._connection_handler,
            "topic": "connections"
        },
        {
            "handler": self._credential_handler,
            "topic": "issue_credential"
        },
        {
            "handler": self._q_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._r_messages_handler,
            "topic": "basicmessages"
        },
        {
            "handler": self._proof_handler,
            "topic": "present_proof"
        }]

        self.agent_controller.register_listeners(l)
    
    # connect with other agents given the initation
    def connect_with(self,invitation, sender):
        #options
        auto_accept="false"
        alias = sender
        loop = asyncio.get_event_loop()
        # receive connection
        invite_response = loop.run_until_complete(self.agent_controller.connections.receive_invitation(invitation, alias, auto_accept))
        connection_id = invite_response["connection_id"]
        # save connection_id to dict
        self.connections_dict[sender] = connection_id
        # accept the connection
        # options
        label = alias
        endpoint = None
        accept_response = loop.run_until_complete(self.agent_controller.connections.accept_invitation(connection_id, label, endpoint))
 

        
        


        
    
    ##############################################################
    ##############################################################
    # 
    # Listeners
    # 
    # ##############################################################
    # ##############################################################       

    # Receive connection messages
    def _connection_handler(self, payload):
        loop = asyncio.get_event_loop()
        state = payload['state']
        connection_id = payload["connection_id"]
        their_role = payload["their_role"]
        routing_state = payload["routing_state"]

        print("----------------------------------------------------------")
        print("Connection Webhook Event Received")
        print("Connection ID : ", connection_id)
        print("State : ", state)
        print("Routing State : ", routing_state)
        print("Their Role : ", their_role)
        print("----------------------------------------------------------")

        if state == "invitation":
            print("invited")
        elif state == "request":
            print("Accepting request ", connection_id)
        elif state == "response":
            print("responded")
        elif state == "active":
            time.sleep(2)
            print("Sending trust ping", connection_id)
            loop.run_until_complete(self.agent_controller.messaging.trust_ping(connection_id, "hello"))
            print(colored("Connection ID: {0} is now active.".format(
                connection_id), "green", attrs=["bold"]))

    # handle pk messages
    def _pk_messages_handler(self,payload):
        loop = asyncio.get_event_loop()
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json['type'] == 'public_key':
                print("Public Key Message Handler", payload, connection_id)
                pk_file = "publickey.txt"
                pk = content_json["data"]
                try:
                    f = open(pk_file, "w+")
                    json.dump(pk, f)
                    f.close()
                    print("public key saved to file: publickey.txt")
                    self.pk_file = pk_file
                    self.pk = pk
                    # send pk to org1 and org2 
                    jpk = json.dumps(content_json)
                    response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["org1"], content))
                    response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["org2"], content))
                    print(f'Public key sent to org1 and org2')
                except Exception as e:
                    print("Error writing file", e)
                    return
        except Exception as e:
            #print("message not handeled by pk message handler")
            return
        
        
    # handle ack messages from org1 and org2
    def _ack_messages_handler(self,payload):
        loop = asyncio.get_event_loop()
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json['type'] == 'ack':
                print("message handeled by ack message handler")
                if connection_id == self.connections_dict["org1"]:
                    self.ack[0] = 1
                    print("org1 acked")
                if connection_id == self.connections_dict["org2"]:
                    print("org2 acked")
                    self.ack[1] = 1
                if (self.ack[0] == 1) and (self.ack[1] == 1):
                    print("both org1 and org2 acked")
                    # do maf
                    self.maf()
                    # send result
                    res = self.create_result_message()
                    response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["researcher"], res))
        except Exception as e:
            print("not handeled by ack_handler")
            return
    
    # handle ack messages from org1 and org2
    def _q_messages_handler(self,payload):
        loop = asyncio.get_event_loop()
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json['type'] == 'query':
                    # send query
                    response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["org1"], content))
        except Exception as e:
            print("not handeled by q_handler")
            return
        
    # handle ack messages from org1 and org2
    def _r_messages_handler(self,payload):
        loop = asyncio.get_event_loop()
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json['type'] == 'r':
                    # send query
                    response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["researcher"], content))
        except Exception as e:
            print("not handeled by r_handler")
            return

    # send pk to registered org connections
    def send_pk(self):
        loop = asyncio.get_event_loop()
        response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["org1"], self.pk))
        response = loop.run_until_complete(self.agent_controller.messaging.send_message(self.connections_dict["org2"], self.pk))
        print(f'Public key sent to org1 and org2')
        
    # listener to recieve phe data 
    def _phe_messages_handler(self,payload):
        connection_id = payload["connection_id"]
        content = payload["content"]
        try:
            content_json = json.loads(content)
            if content_json["type"] == "phe":
                print("PHE encrypted data Message Handler", payload, connection_id)
                phe_file = "./enc_datasets/phe_" + connection_id + ".txt"
                phe_data = content_json["data"]
                try:
                    f = open(phe_file, "a")
                    d = json.dumps(phe_data)
                    f.write(d)
                    f.close()
                except Exception as e:
                    print("Error writing file", e)
                    return
        except Exception as e:
            #print("message not handeled by phe message handler")
            return

    # handle credential messages
    def _credential_handler(self, payload):
        loop = asyncio.get_event_loop()
        connection_id = payload['connection_id']
        exchange_id = payload['credential_exchange_id']
        state = payload['state']
        role = payload['role']
        print("\n---------------------------------------------------\n")
        print("Handle Issue Credential Webhook")
        print(f"Connection ID : {connection_id}")
        print(f"Credential exchange ID : {exchange_id}")
        print("Agent Protocol Role : ", role)
        print("Protocol State : ", state)
        print("\n---------------------------------------------------\n")
        print("Handle Credential Webhook Payload")

        if state == "offer_received":
            proposal = payload["credential_proposal_dict"]
            attributes = proposal['credential_proposal']['attributes']
            print(f"Offering: {attributes}")
            print("Sending request for credential")
            loop.run_until_complete(self.agent_controller.issuer.send_request_for_record(exchange_id))
        elif state == "request_sent":
            print("\n Request Sent")
        elif state == "credential_received":
            print("Storing Credential ...")
            loop.run_until_complete(self.agent_controller.issuer.store_credential(exchange_id, credential_id)) 

        elif state == "credential_acked":
            credential = payload["credential"]
            print("Credential Stored\n")
            print(credential)
            credential_referent = credential["referent"]
            self.credentials.append(credential_referent)
            print("Referent", credential_referent)

    # handle proof messages
    def _proof_handler(self, payload):
        role = payload["role"]
        connection_id = payload["connection_id"]
        pres_ex_id = payload["presentation_exchange_id"]
        state = payload["state"]
        print("\n---------------------------------------------------------------------\n")
        print("Handle present-proof")
        print("Connection ID : ", connection_id)
        print("Presentation Exchange ID : ", pres_ex_id)
        print("Protocol State : ", state)
        print("Agent Role : ", role)
        print("Initiator : ", payload["initiator"])
        print("\n---------------------------------------------------------------------\n")

        if state == "request_received":
            presentation_request = payload["presentation_request"]
            print("Recieved Presentation Request\n")
            print("\nRequested Attributes - Note the restrictions. These limit the credentials we could respond with\n")
            print(presentation_request["requested_attributes"])
        elif state == "presentation_sent":
            print("Presentation sent\n")

        elif state == "presentation_acked":
            print("Presentation has been acknowledged by the Issuer")

    
    def maf(self):
        import utils

        time_start = time.time()

        # Encrypted dataset files , change these to the actual names in 'enc_datasets' folder
        data1_name = f'./enc_datasets/phe_{self.connections_dict["org1"]}.txt'
        data2_name = f'./enc_datasets/phe_{self.connections_dict["org2"]}.txt'
        output_name = f'allele_count_{self.connections_dict["researcher"]}.txt'
        self.result_file = output_name
        rows = 1
        num_elem = self.n

        # read the public key file
        print("Reading encrypted files...")
        file = open("publickey.txt")
        line = file.read().replace("\n", " ")
        file.close()
        received_dict = json.loads(line)
        pk = received_dict['public_key']
        public_key_rec = paillier.PaillierPublicKey(n=int(pk['n']))

        # opening the encrypted datasets in the correct order
        case1 = utils.opening_datasets_in_order(data1_name, public_key_rec)
        case2 = utils.opening_datasets_in_order(data2_name, public_key_rec)

        public_key = public_key_rec
        case1 = np.asarray(case1)
        case2 = np.asarray(case2)
        print(np.shape(case1))
        print(np.shape(case2))
        he_case1 = case1.reshape((rows,num_elem,2))
        he_case2 = case2.reshape((rows,num_elem,2))

        # calculate the allele counts for the given number of rows
        AF_list = []
        for i in range(rows):
            af = utils.AF(i, he_case1,he_case2)
            AF_list.append(af)

        #print("Calculated encrypted allele counts")
        AF_list = np.asarray(AF_list)


        #save encrypted counts of allels into a file
        enc_with_one_pub_key = {}
        enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in AF_list]
        serialised2 = json.dumps(enc_with_one_pub_key)
        text_file = open(output_name, "w")
        text_file.write(serialised2)
        text_file.close()
        #print("Saved encrypted allel counts in the file {}".format(output_name))

        # timing
        time_end = time.time()
        print('time cost = {} s'.format(time_end-time_start))

    # Create public key to use for the encryption
    def create_result_message(self):
        result_file = self.result_file
        send_data_final = None
        try:
            f = open(result_file, "r")
            print("results file opened")
            result = f.read()
            result_json = json.loads(result)
            send_data = {"type":"result"}
            send_data["data"] = result_json
            send_data_final = json.dumps(send_data)    
            f.close()
        except:
            print("Unable to open file")
        print(send_data_final)
        return send_data_final