# Listener functions

from termcolor import colored
import json

# Receive connection messages
def connections_handler(payload):
    state = payload['state']
    connection_id = payload["connection_id"]
    their_role = payload["their_role"]
    routing_state = payload["routing_state"]
    
    print("----------------------------------------------------------")
    print("Connection Webhook Event Received")
    print("Connection ID : ", connection_id)
    print("State : ", state)
    print("Routing State : ", routing_state)
    print("Their Role : ", their_role)
    print("----------------------------------------------------------")

    if state == "invitation":
        # Your business logic
        print("invitation")
    elif state == "request":
        # Your business logic
        print("request")

    elif state == "response":
        # Your business logic
        print("response")
    elif state == "active":
        # Your business logic
        print(colored("Connection ID: {0} is now active.".format(connection_id), "green", attrs=["bold"]))

# handle pk messages
def _pk_messages_handler(payload):
    connection_id = payload["connection_id"]
    content = payload["content"]
    try:
        content_json = json.loads(content)
        if content_json['type'] == 'public_key':
            print("Public Key Message Handler", payload, connection_id)
            pk_file = "publickey.txt"
            pk = content_json["data"]
            try:
                f = open(pk_file, "w+")
                json.dump(pk, f)
                f.close()
                print("public key saved to file: publickey.txt")
            except Exception as e:
                print("Error writing file", e)
                return
    except Exception as e:
        print("message not handeled by pk message handler")
        return

def phe_messages_handler(payload):
    connection_id = payload["connection_id"]
    content = payload["content"]
    try:
        content_json = json.loads(content)
        if content_json["type"] == "phe":
            print("PHE encrypted data Message Handler", payload, connection_id)
            phe_file = "./enc_datasets/phe_" + connection_id + ".txt"
            phe_data = content_json["data"]
            try:
                f = open(phe_file, "a")
                d = json.dumps(phe_data)
                f.write(d)
                f.close()
            except Exception as e:
                print("Error writing file", e)
                return
    except Exception as e:
        print("message not handeled by phe message handler")
        return

phe_message_listener = {
        "handler": phe_messages_handler,
        "topic": "basicmessages"
}