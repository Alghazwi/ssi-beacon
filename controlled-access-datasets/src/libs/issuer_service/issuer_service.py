import os
import requests
import json
import sys
import time
import asyncio
from aries_cloudcontroller import AriesAgentController
from datetime import date
import nest_asyncio

nest_asyncio.apply()

# Issuer service to issue specific type of credential 
# modified version of the issuer service in https://github.com/wip-abramson/aries-issuer-service

class IssuerService:
    def __init__(self, agent_config, cred_config):
        self.schema_id = None
        self.cred_def_id = None
        self.cred_config = cred_config
        loop = asyncio.get_event_loop()
        self.agent_controller = agent_controller = AriesAgentController(agent_config["admin_url"],agent_config["api_key"])
        loop.run_until_complete(self.agent_controller.init_webhook_server(agent_config["webhook_host"], agent_config["webhook_port"]))
        whost = agent_config["webhook_host"]
        wport = agent_config["webhook_port"]
        print(f"Listening for webhooks from agent at http://{whost}:{wport}")
        
        self.register_listeners()
        response = loop.run_until_complete(self.agent_controller.wallet.get_public_did())

        print("PUBLIC DID", response)
        if not response['result']:
            did = loop.run_until_complete(self.write_public_did())
            print(f"Public DID {did} written to the ledger")

            ###### Credential and Definition ##########
            # Schema details
            schema_name = cred_config["schema_name"]
            # version of the schema 
            schema_version = cred_config["schema_version"]
            # a list of credential attributes 
            attributes = cred_config["attributes"]
            # tag for the credential
            tag = cred_config["tag"]
            # Write Cred Def and Schema to ledger
            self.create_schema_and_definition(schema_name, schema_version, attributes, tag)

            print(f"Credential Definition {self.cred_def_id} for schema {self.schema_id}")

        # Create multi-use Invitation
        invite = loop.run_until_complete( self.agent_controller.connections.create_invitation(multi_use="true"))
        print("Multi Use Connection Invitation")
        print(invite["invitation"])
        

    def register_listeners(self):
        listeners = [{
            "topic": "issue_credential",
            "handler": self.cred_handler
        },
        {
            "handler": self.connections_handler,
            "topic": "connections"
        }]
        self.agent_controller.register_listeners(listeners)

    # create credential schema and definition
    def create_schema_and_definition(self,schema_name, schema_version, attributes, tag):
        loop = asyncio.get_event_loop()
        # create schema
        response = loop.run_until_complete(self.agent_controller.schema.write_schema(schema_name, attributes, schema_version))
        self.schema_id = response["schema_id"]
        # create definition
        support_revocation = False
        cred_def_response = loop.run_until_complete(self.agent_controller.definitions.write_cred_def(self.schema_id, tag, support_revocation))
        self.cred_def_id = cred_def_response["credential_definition_id"]

    def cred_handler(self, payload):
        connection_id = payload['connection_id']
        exchange_id = payload['credential_exchange_id']
        state = payload['state']
        role = payload['role']
        print("\n---------------------------------------------------\n")
        print("Handle Issue Credential Webhook")
        print(f"Connection ID : {connection_id}")
        print(f"Credential exchange ID : {exchange_id}")
        print("Agent Protocol Role : ", role)
        print("Protocol State : ", state)
        print("\n---------------------------------------------------\n")
        loop = asyncio.get_event_loop()
        if state == "proposal_received":
            print(f'Proposal Comment : {payload["credential_proposal_dict"]["comment"]}')
            proposed_cred_def_id = payload["credential_proposal_dict"]["cred_def_id"]
            proposed_schema_id = payload["credential_proposal_dict"]["schema_id"]
            if proposed_cred_def_id == self.cred_def_id and proposed_schema_id == self.schema_id:
                credential_attributes = payload["credential_proposal_dict"]["credential_proposal"]["attributes"]
                issue_date = date.today().isoformat()
                date_dict = {"name": "Date", "value": issue_date}
                credential_attributes.append(date_dict)
                print(credential_attributes)
                trace = False
                comment = ""
                auto_remove = False

                loop.run_until_complete(
                    self.agent_controller.issuer.send_credential(connection_id, self.schema_id, self.cred_def_id, credential_attributes,
                                                            comment, auto_remove, trace))
            else:
                explanation = "You have not referenced the correct schema and credential definitions"
                loop.run_until_complete(self.agent_controller.issuer.send_problem_report(exchange_id, explanation))


    def connections_handler(payload):
        state = payload['state']
        connection_id = payload["connection_id"]
        their_role = payload["their_role"]
        routing_state = payload["routing_state"]

        print("----------------------------------------------------------")
        print("Connection Webhook Event Received")
        print("Connection ID : ", connection_id)
        print("State : ", state)
        print("Routing State : ", routing_state)
        print("Their Role : ", their_role)
        print("----------------------------------------------------------")

        print(payload)

        if state == "active":
            # Your business logic
            print("Connection ID: {0} is now active.".format(connection_id))


    async def write_public_did(self):
        # generate new DID
        response = await self.agent_controller.wallet.create_did()

        did_object = response['result']
        did = did_object["did"]
        print("New DID", did)
        # write new DID to Sovrin Stagingnet

        url = 'https://selfserve.sovrin.org/nym'

        payload = {"network": "stagingnet", "did": did_object["did"], "verkey": did_object["verkey"], "paymentaddr": ""}

        # Adding empty header as parameters are being sent in payload
        headers = {}

        r = requests.post(url, data=json.dumps(payload), headers=headers)
        if r.status_code != 200:
            print("Unable to write DID to StagingNet")
            raise Exception

        response = await self.agent_controller.ledger.get_taa()
        taa = response['result']['taa_record']
        taa['mechanism'] = "service_agreement"

        await self.agent_controller.ledger.accept_taa(taa)

        await self.agent_controller.wallet.assign_public_did(did)

        return did_object["did"]

    def populate_attributes(self, payload):
        name = None
        info = None
        role = None
        affiliation = None
        for attribute in payload["credential_proposal_dict"]["credential_proposal"]["attributes"]:
            if attribute["name"] == "Name":
                name = attribute["value"]
            elif attribute["name"] == "Contact Info":
                info = attribute["value"]
            elif attribute["name"] == "Role":
                role = attribute["value"]
            elif attribute["name"] == "Affiliation":
                affiliation = attribute["value"]

        issue_date = date.today().isoformat()
        credential_attributes = [
            {"name": "Name", "value": name},
            {"name": "Contact Info", "value": info},
            {"name": "Role", "value": role},
            {"name": "Affiliation", "value": affiliation},
            {"name": "Date", "value": issue_date}
        ]


        

