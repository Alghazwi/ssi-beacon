######################################################################################
#############   Aries ACA-Py ACC-Py Jupyter Playground Configuration   ###############
######################################################################################
version: "3"
services:
  #########################################################################
  #### org Services ######
  #########################################################################
  ### wallet-db
  org-wallet-db:
    image: postgres:11
    container_name: org-wallet-db
    command: postgres -c listen_addresses='*'
    env_file:
      - playground/org/.env
    volumes:
      - ./resources/init.sql:/docker-entrypoint-initdb.d/init.sql
    networks:
      - org-domain
    tty: true
  #### ngrok
  ngrok-org:
    image: wernight/ngrok
    env_file:
      - playground/org/.env
    command: ngrok http org-agent:3020 --log stdout
    networks:
      - org-domain
  ### agent
  org-agent:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.agent
    env_file:
      - playground/org/.env
    ports:
      # HTTP_PORT
      - 9020:3020
      # ADMIN_PORT
      - 9021:3021
    depends_on:
      - org-wallet-db
    networks:
      - org-domain
  ### business-logic
  org-business-logic:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.controller
    env_file:
      - playground/org/.env
    depends_on:
      - org-agent
    networks:
      - org-domain
    volumes:
      - ./playground/org/notebooks:/workspace
      - ./libs:/workspace/libs
    ports:
      - 8894:8888
      # WEBHOOK_PORT
      - 9010:3010

  #########################################################################
  #### dataset1 Services ######
  #########################################################################
  # wallet-db
  # a postgres database where agent data will be persisted unless you run ./manage down
  # ./manage stop with stop the containers but persist the database volumes
  dataset1-wallet-db:
    image: postgres:11
    container_name: dataset1-wallet-db
    command: postgres -c listen_addresses='*'
    # env file defines
    env_file:
      - playground/dataset1/.env
    volumes:
      - ./resources/init.sql:/docker-entrypoint-initdb.d/init.sql
    networks:
      - dataset1-domain
    tty: true
  #### ngrok
  ngrok-dataset1:
    image: wernight/ngrok
    env_file:
      - playground/dataset1/.env
    command: ngrok http dataset1-agent:3020 --log stdout
    networks:
      - dataset1-domain
  ### agent
  dataset1-agent:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.agent
    env_file:
      - playground/dataset1/.env
    ports:
      # HTTP_PORT
      - 4020:3020
      # ADMIN_PORT
      - 4021:3021
    depends_on:
      - dataset1-wallet-db
    networks:
      - dataset1-domain
  ### business-logic
  dataset1-business-logic:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.controller
    env_file:
      - playground/dataset1/.env
    depends_on:
      - dataset1-agent
    networks:
      - dataset1-domain
    volumes:
      - ./playground/dataset1/notebooks:/workspace
      - ./libs:/workspace/libs
    ports:
      - 8889:8888
      # WEBHOOK_PORT
      - 4010:3010
  #########################################################################
  #### dataset2 Services ######
  #########################################################################
  dataset2-wallet-db:
    image: postgres:11
    container_name: dataset2-wallet-db
    command: postgres -c listen_addresses='*'
    env_file:
      - playground/dataset2/.env
    volumes:
      - ./resources/init.sql:/docker-entrypoint-initdb.d/init.sql
    networks:
      - dataset2-domain
    tty: true
  ngrok-dataset2:
    image: wernight/ngrok
    env_file:
      - playground/dataset2/.env
    command: ngrok http dataset2-agent:3020 --log stdout
    networks:
      - dataset2-domain
  dataset2-agent:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.agent
    env_file:
      - playground/dataset2/.env
    ports:
      # HTTP_PORT
      - 7020:3020
      # ADMIN_PORT
      - 7021:3021
    depends_on:
      - dataset2-wallet-db
    networks:
      - dataset2-domain
  dataset2-business-logic:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.controller
    env_file:
      - playground/dataset2/.env
    depends_on:
      - dataset2-agent
    networks:
      - dataset2-domain
    volumes:
      - ./playground/dataset2/notebooks:/workspace
      - ./libs:/workspace/libs
    ports:
      - 8892:8888
      # WEBHOOK_PORT
      - 7010:3010

  #########################################################################
  #### beacon Services ######
  #########################################################################
  beacon-wallet-db:
    image: postgres:11
    container_name: beacon-wallet-db
    command: postgres -c listen_addresses='*'
    env_file:
      - playground/beacon/.env
    volumes:
      - ./resources/init.sql:/docker-entrypoint-initdb.d/init.sql
    networks:
      - beacon-domain
    tty: true
  ngrok-beacon:
    image: wernight/ngrok
    env_file:
      - playground/beacon/.env
    command: ngrok http beacon-agent:3020 --log stdout
    networks:
      - beacon-domain
  beacon-agent:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.agent
    env_file:
      - playground/beacon/.env
    ports:
      - 3020:3020
      - 3021:3021
    depends_on:
      - beacon-wallet-db
    networks:
      - beacon-domain
  beacon-business-logic:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.controller
    env_file:
      - playground/beacon/.env
    depends_on:
      - beacon-agent
    networks:
      - beacon-domain
    volumes:
      - ./playground/beacon/notebooks:/workspace
      - ./libs:/workspace/libs
    ports:
      - "8888:8888"
      - 3010:3010
  #########################################################################
  #### Researcher Services ######
  #########################################################################
  researcher-wallet-db:
    image: postgres:11
    container_name: researcher-wallet-db
    command: postgres -c listen_addresses='*'
    env_file:
      - playground/researcher/.env
    volumes:
      - ./resources/init.sql:/docker-entrypoint-initdb.d/init.sql
    networks:
      - researcher-domain
    tty: true
  ngrok-researcher:
    image: wernight/ngrok
    command: ngrok http researcher-agent:3020 --log stdout
    networks:
      - researcher-domain
  researcher-agent:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.agent
    env_file:
      - playground/researcher/.env
    ports:
      # HTTP_PORT
      - 5020:3020
      # ADMIN_PORT
      - 5021:3021
    depends_on:
      - researcher-wallet-db
    networks:
      - researcher-domain
  researcher-business-logic:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.controller
    env_file:
      - playground/researcher/.env
    depends_on:
      - researcher-agent
    networks:
      - researcher-domain
    volumes:
      - ./playground/researcher/notebooks:/workspace
      - ./libs:/workspace/libs
    ports:
      - 8890:8888
      # WEBHOOK_PORT
      - 5010:3010

#########################################################################
#### Researcher Credential Authority Services ######
#########################################################################
  issuer-wallet-db:
    image: postgres:11
    container_name: issuer-wallet-db
    command: postgres -c listen_addresses='*'
    # env file defines
    env_file:
      - playground/issuer/.env
    volumes:
      - ./resources/init.sql:/docker-entrypoint-initdb.d/init.sql
    networks:
      - issuer-domain
    tty: true
  ngrok-issuer:
    image: wernight/ngrok
    command: ngrok http issuer-agent:3020 --log stdout
    networks:
      - issuer-domain
  issuer-agent:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.agent
    env_file:
      - playground/issuer/.env
    ports:
      # HTTP_PORT
      - 6020:3020
      # ADMIN_PORT
      - 6021:3021
    depends_on:
      - issuer-wallet-db
    networks:
      - issuer-domain
  issuer-business-logic:
    build:
      context: .
      dockerfile: dockerfiles/Dockerfile.controller
    env_file:
      - playground/issuer/.env
    depends_on:
      - issuer-agent
    networks:
      - issuer-domain
    volumes:
      - ./playground/issuer/notebooks:/workspace
      - ./libs:/workspace/libs
    ports:
      - 8891:8888
      # WEBHOOK_PORT
      - 6010:3010

##### UNCOMMENT FOR A TAILS SERVER ######
# Note - for revocation to work you will have to make additional changes to the env files
#  tails-server:
#    env_file:
#      - playground/beacon/.env
#    build:
#      context: https://github.com/bcgov/indy-tails-server.git
#      dockerfile: docker/Dockerfile.tails-server
#    ports:
#      - 6543:6543
#    networks:
#      - beacon-domain
#      - dataset1-domain
#    command: >
#      tails-server
#        --host 0.0.0.0
#        --port 6543
#        --storage-path /tmp/tails-files
#        --log-level INFO
networks:
  beacon-domain:
  dataset1-domain:
  dataset2-domain:
  researcher-domain:
  issuer-domain:
  org-domain:
