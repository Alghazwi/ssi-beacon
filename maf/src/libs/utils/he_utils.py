import json
from datetime import datetime
import asyncio
from phe import paillier
import time
import pickle
import numpy as np
import re
import os
import sys
import time
from termcolor import colored



#################################################
########### Create PK ##################
#################################################
# Create public key to use for the encryption
def create_pk(pk_file, sk_file):
    # generate the public and secret key pair
    public_key, secret_key = paillier.generate_paillier_keypair() 

    # saving the public key in a text file 
    enc_with_one_pub_key = {}
    enc_with_one_pub_key['public_key'] = {'n': public_key.n}
    serialised = json.dumps(enc_with_one_pub_key)
    text_file = open(pk_file, "w")
    text_file.write(serialised)
    text_file.close()

    # saving the secret key in a text file 
    enc_with_one_sec_key = {}
    enc_with_one_sec_key['secret_key'] = {'p': secret_key.p, 'q': secret_key.q }
    serialised = json.dumps(enc_with_one_sec_key)
    text_file = open(sk_file, "w")
    text_file.write(serialised)
    text_file.close()

    #print
    response = "key pair created and saved"
    print(response)

#################################################
########### Encode and Encrypt ##################
#################################################
def encode_encrypt(pk_file, dataset_file, enc_dataset_file, ref_alt, n, rows: int = 1):
    # Timing
    time_start = time.time()
    # get the public key from the file pk_file
    file = open(pk_file)
    line = file.read().replace("\n", " ")
    file.close()

    received_dict = json.loads(line)
    pk = received_dict['public_key']
    public_key = paillier.PaillierPublicKey(n=int(pk['n']))

    # names of the input and output files
    data_file = dataset_file
    enc_data_file = enc_dataset_file

    # read the dataset in dataset_file with n number of elements as defined earlier 
    data = get_data(data_file,num_elem=n)

    # map the two dataset entries to integers
    map_c, ref, alt = map_data(data,num_elem=n,row_num=1)

    # save the ref and alt
    ref_alt_file = ref_alt
    file = open(ref_alt_file, 'wb')
    pickle.dump([ref,alt], file)
    file.close()

    # timing
    time_end = time.time()
    print('Execution time encoding dataset 1 = {} s'.format(time_end-time_start))

    time_start = time.time()

    # encrypt the dataset for the number of rows indicated 
    he_data = he_data_row(map_c, public_key)
    he_data = np.asarray(he_data)
    he_data_flat = he_data.ravel()

    enc_with_one_pub_key = {}
    enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in he_data_flat]
    serialised2 = json.dumps(enc_with_one_pub_key)
    text_file = open(enc_data_file, "w")
    text_file.write(serialised2)
    text_file.close()
    print("Enctypted dataset saved it file {}\n".format(enc_data_file))

    # timing
    time_end = time.time()
    print('Execution time encrypting dataset 1 = {} s'.format(time_end-time_start))

def get_data(path,num_elem):

    """
    Function to open the .txt files of the datasets and put the data into a numpy array with 101 columns.

    Parameters:
        path (str): The path + file name of the dataset.

    Output: 
        data5 (ndarray): The dataset as a numpy array.
    """
    f = open(path, "r")
    data = f.read()
    data2 = re.split(' |\n|\n ',data)
    data3 = data2[:-1]
    data3.insert(0, 'name')
    data4=np.asarray(data3)
    data5=data4.reshape(312, num_elem+1) #31512 elements in total for both datasets
    return data5
def map_data(data,num_elem,row_num):
    """
    Function that returns the data mapped to integres (100 is the number of samples in each row - should be changed if the dataset is different)

    Parameters:
        data (ndarray): The dataset that we want to map to integers.
        num_elem (int): Number of individuals used in the dataset (so the number of samples).

    Output: 
        m_data (ndarray): The dataset with each element mapped to integers.
    """
    m_data = data[1:,1:].copy()
    row=m_data[row_num] # take the row we want

    if (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AG')>0) or (np.count_nonzero(row == 'AG')>0 and np.count_nonzero(row == 'GG')>0):
        ref = 0 # the corresponding ref and alt indicated with an integer
        alt = 3
        for i in range(num_elem):
            if row[i] == "AA":
                row[i]=0
            elif row[i] == 'AG':
                row[i]=1
            elif row[i] == 'GG':
                row[i]=2
    elif (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AT')>0) or (np.count_nonzero(row == 'AT')>0 and np.count_nonzero(row == 'TT')>0):
        ref = 0
        alt = 1
        for i in range(num_elem):
            if row[i] == "AA":
                row[i]=0
            elif row[i] == 'AT':
                row[i]=1
            elif row[i] == 'TT':
                row[i]=2
    elif (np.count_nonzero(row == 'AA')>0 and np.count_nonzero(row == 'AC')>0) or (np.count_nonzero(row == 'AC')>0 and np.count_nonzero(row == 'CC')>0):
        ref = 0
        alt = 2
        for i in range(num_elem):
            if row[i] == "AA":
                row[i]=0
            elif row[i] == 'AC':
                row[i]=1
            elif row[i] == 'CC':
                row[i]=2
    elif (np.count_nonzero(row == 'GG')>0 and np.count_nonzero(row == 'GT')>0) or (np.count_nonzero(row == 'GT')>0 and np.count_nonzero(row == 'TT')>0):
        ref = 3
        alt = 1
        for i in range(num_elem):
            if row[i] == "GG":
                row[i]=0
            elif row[i] == 'GT':
                row[i]=1
            elif row[i] == 'TT':
                row[i]=2
    elif (np.count_nonzero(row == 'CC')>0 and np.count_nonzero(row == 'CT')>0) or  (np.count_nonzero(row == 'CT')>0 and np.count_nonzero(row == 'TT')>0):
        ref = 2
        alt = 1
        for i in range(num_elem):
            if row[i] == "CC":
                row[i]=0
            elif row[i] == 'CT':
                row[i]=1
            elif row[i] == 'TT':
                row[i]=2
    elif (np.count_nonzero(row == 'CC')>0 and np.count_nonzero(row == 'CG')>0) or (np.count_nonzero(row == 'CG')>0 and np.count_nonzero(row == 'GG')>0):
        ref = 2
        alt = 3
        for i in range(num_elem):
            if row[i] == "CC":
                row[i]=0
            elif row[i] == 'CG':
                row[i]=1
            elif row[i] == 'GG':
                row[i]=2

    return [row.astype(int), ref, alt]
# function that performs the homomorphic encryption of the dataset after the entries have been mapped to integers
def he_data_row(data, public_key):
    """
    Function that returns the dataset encrypted.

    Parameters:
        data (ndarray): The dataset that we want to enctypt.

    Output: 
        cyphertexts (list): The dataset with each element mapped to integers.
    """
    cyphertexts = []
    for elem in data:
        gen = [public_key.encrypt(0),public_key.encrypt(0)]
        if elem == 1:
            gen[elem] = public_key.encrypt(1)
        elif elem == 0:
            gen[elem] = public_key.encrypt(2)

        cyphertexts.append(gen)
    return cyphertexts

######################################################
########### Perform MAF Calculation ##################
######################################################

def maf(pk_file, dataset1_file, dataset2_file, output_file, n, rows: int=1):
    # Timing 
    time_start = time.time()

    # Files and Attributes
    data1_name = dataset1_file
    data2_name = dataset2_file
    output_name = output_file
    num_elem = n

    # read the public key file
    print("Reading encrypted files...")
    file = open(pk_file)
    line = file.read().replace("\n", " ")
    file.close()
    received_dict = json.loads(line)
    pk = received_dict['public_key']
    public_key_rec = paillier.PaillierPublicKey(n=int(pk['n']))

    # opening the encrypted datasets in the correct order
    case1 = open_dataset(data1_name, public_key_rec)
    case2 = open_dataset(data2_name, public_key_rec)

    public_key = public_key_rec
    case1 = np.asarray(case1)
    case2 = np.asarray(case2)
    print(np.shape(case1))
    print(np.shape(case2))
    he_case1 = case1.reshape((rows,num_elem,2))
    he_case2 = case2.reshape((rows,num_elem,2))

    # calculate the allele counts for the given number of rows
    AF_list = []
    for i in range(rows):
        af = AF(i, he_case1,he_case2)
        AF_list.append(af)

    #print("Calculated encrypted allele counts")
    AF_list = np.asarray(AF_list)


    #save encrypted counts of allels into a file
    enc_with_one_pub_key = {}
    enc_with_one_pub_key['values'] = [(str(x.ciphertext()), x.exponent) for x in AF_list]
    serialised2 = json.dumps(enc_with_one_pub_key)
    text_file = open(output_name, "w")
    text_file.write(serialised2)
    text_file.close()
    #print("Saved encrypted allel counts in the file {}".format(output_name))

    # timing
    time_end = time.time()
    print('time cost = {} s'.format(time_end-time_start))

def open_dataset(dataset_name, public_key_rec):
    file = open(dataset_name)
    enc_data = file.read().replace("\n", " ")
    file.close()
    received_dict = json.loads(enc_data)
    case = [paillier.EncryptedNumber(public_key_rec, int(x[0]), int(x[1])) for x in received_dict['data']]
    return case
# opening the encrypted datasets in the correct order
def opening_datasets_in_order(dataset_name, public_key_rec):
    file = open(dataset_name)
    data = file.read().replace("}{", "} , {") # adding a place to split the lines in the next step
    data2 = re.split(' , ',data) # split the lines

    # make a dict of dicts
    d = {}
    for elem in data2:
        x = json.loads(elem)
        d[x["index"]] = x["value"] # set the index value as the key of the subdict
    
    # sorting the dict of dicts according to the index value
    sorted_d = sorted(d.items(), key=lambda x: x[0], reverse=False)
    
    # make a dict from the sorted dict to match the form of the data before
    d_final = {}
    d_final["values"] = []
    for elem in sorted_d:
        d_final["values"].append(elem[1])
    
    case = [paillier.EncryptedNumber(public_key_rec, int(x[0]), int(x[1])) for x in d_final['values']]
    return case

def AF(row_num, data1, data2):

    """
    Function to calculate the allel counts incorporating all other functions.

    Parameters:
        row_num (int): The number of rows from the datasets that we want to calculate the allele frequency for.
        data1 (ndarray): The first encrypted dataset.
        data2 (ndarray): The second encrypted dataset.

    Output: 
        allel_count (list): The count of each of the alleles.
    """

    # merge the two rows for this same genotype
    merged = data1[row_num] + data2[row_num]
    
    # calculate the relative genotypic count
    gen_count = he_genotype_count(merged)
    
    # calculate the allel counts
    result = he_allel_count(gen_count)

    return result

def he_genotype_count(cyphertexts):

    """
    Function to count the relative genotypic count.

    Parameters:
        cyphertexts (ndarray): The relative genotypic count.

    Output: 
        sum_list (list): The relative genotypic count.
    """

    sum_list = []
    for i in range(2):
        summ=0
        for x in cyphertexts:
            summ+=x[i]
        sum_list.append(summ)
    return sum_list


def he_allel_count(freq):

    """
    Function to count the number of alleles.

    Parameters:
        freq (ndarray): The relative genotypic count, obtained from the output of the function he_genotype_count().

    Output: 
        allel_count (int): The count of one of the alleles.
    """

    allel_count = freq[0]+freq[1]
    return allel_count


#################################################
########### Decrypt the result ##################
#################################################

def decrypt(pk_file, sk_file, ref_alt, results_file, n, rows: int=1):
    # Timing
    time_start = time.time()

    # results file
    allele_file = results_file
    num_elem = n

    # get the public key from the file publickey.txt
    file = open(pk_file)
    line = file.read().replace("\n", " ")
    file.close()

    received_dict = json.loads(line)
    pk = received_dict['public_key']
    public_key = paillier.PaillierPublicKey(n=int(pk['n']))

    # get the secret key from the file secretkey.txt
    file = open(sk_file)
    line = file.read().replace("\n", " ")
    file.close()

    received_dict = json.loads(line)
    pk = received_dict['secret_key']
    secret_key = paillier.PaillierPrivateKey(public_key, p=int(pk['p']),q=int(pk['q']))

    # get the encrypted allele counts from the file with the encrypted results
    file = open(allele_file)
    line2 = file.read().replace("\n", " ")
    file.close()
    received_dict = json.loads(line2)
    al_count_flat = [paillier.EncryptedNumber(public_key, int(x[0])) for x in received_dict['values']]

    # get the ref and alt
    file = open(ref_alt, 'rb')
    ref_alt = pickle.load(file)
    file.close()


    outcome = decryption_and_MAF(al_count_flat, secret_key, ref_alt, num_elem)
    print(outcome)
    # timing
    time_end = time.time()
    print('execution time decryption and final calculation of the MAF = {} s'.format(time_end-time_start))

# function to decrypt the allele counts and output the MAF
def decryption_and_MAF(al_count, secret_key, ref_alt, num_elem):
    """
    Function to decrypt the allele count and return the final MAF result.

    Parameters:
        allele_counts (ndarray): The encrypted allele counts using the combined datasets.

    Output: 
        result (str): The minimum allele frequency written as a fraction over the total number of alleles.
    """ 
    
    a = [secret_key.decrypt(x) for x in al_count][0]

    ref_alt_str = []
    for i in range(len(ref_alt)):
        if ref_alt[i] == 0:
            ref_alt_str.append('A')
        elif ref_alt[i] == 1:
            ref_alt_str.append('T')
        elif ref_alt[i] == 2:
            ref_alt_str.append('C')
        elif ref_alt[i] == 3:
            ref_alt_str.append('G')
    
    if a<(num_elem*4-a):
        maf = f"The MAF is {a}/{num_elem*4} for allele {ref_alt_str[0]}."
    elif a>(num_elem*4-a):
        maf = f"The MAF is {num_elem*4-a}/{num_elem*4} for allele {ref_alt_str[1]}."
    else:
        maf = f"There are exactly the same amount of {ref_alt_str[0]} and {ref_alt_str[1]}."

    return maf



